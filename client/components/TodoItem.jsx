import React, { Component } from "react";

class TodoItem extends Component {
  render() {
    const task = this.props.task;
    const id = this.props.id;
    const isChecked = this.props.isChecked;
     var cls = isChecked ? "checked": "unchecked";
    return (
      <div className="container">
        <div className="item">
        <input 
          onChange={event => this.props.onChecked(this.props.id, event)}
          type="checkbox"
          id={id}
        />
        </div>
        <div className="item">
        <span className ={cls}  id={task}>{task}</span>
        </div>
        <div className="item">
          <button
            onClick={() => this.props.onDelete(this.props.id)}
            className="deleteBtn"
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
}

export default DeleteTask;
