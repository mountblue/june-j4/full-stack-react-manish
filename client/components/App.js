import React, { Component } from "react";
import TodoInput from "./TodoInput.jsx";
import TodoItem from "./TodoItem.jsx";
import axios from "axios";
var querystring = require("querystring");
import "../css/App.css";
import uuid from "uuid";

class App extends Component {
  constructor() {
    super();
    this.state = {
      todoTasks: [{ text: "a", isChecked: false, _id: "3746287346" }]
    };
  }

  getTask() {
    axios.get("todos").then(response => {
      this.setState({ todoTasks: response.data });
    });
  }

  componentDidMount() {
    this.getTask();
  }
  componentWillMount() {
    this.getTask();
  }

  onAdd = task => {
    console.log(task);

    axios
      .post(
        "/todos",
        querystring.stringify({
          text: task,
          isChecked: false,
          _id: uuid.v4()
        }),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(function(response) {
        console.log(response.data);
        
        // this.setState({
        //   todoTasks : response.data
        // });
      });
      this.componentDidMount();
  };

  onDelete = taskId => {
    console.log(taskId);
    // axios.delete('/delete'+taskId);
    fetch("/todos/"+taskId, {
      method: "DELETE"
    })
      .then(response => response.json())
      .catch(error => console.error("Error:", error))
      .then(this.componentDidMount());
  };

  onChecked = (taskId, event) => {
    
    fetch("/todos/"+taskId, {
      method:"PUT",
      body: JSON.stringify({isChecked:event.target.checked}),
        headers: {
            'Content-Type': 'application/json charset=utf-8'
        }
    })
      .then(response => response.json())
      .catch(error => console.error("Error:", error))
      .then(this.componentDidMount());
  };

  render() {
    return (
      <div>
        <h1>TODO APPLICATION</h1>
        <TodoInput onAdd={this.onAdd} />
        {this.state.todoTasks.map(task => {
          return (
            <TodoItem
              key={task._id}
              task={task.text}
              isChecked={task.isChecked}
              id={task._id}
              onDelete={this.onDelete}
              onChecked={this.onChecked}
            />
          );
        })}
      </div>
    );
  }
}
export default App;
